#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    /** connect slots */
    QObject::connect(ui->actionOpen_CSV, SIGNAL(triggered()),
                                        this, SLOT( open_csv_file() ));
    QObject::connect(ui->pushButton, SIGNAL(released()),
                                        this, SLOT( make_calculation() ));
}


MainWindow::~MainWindow()
{
    if  (bill != nullptr)
    {
        delete bill;
    }
    delete ui;
}


/** Public slot: action called from menu: open csv file
*/
void MainWindow::open_csv_file()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open CSV file"), "",
                                                    tr("CSV file (*.csv)"));
    /** check  if it is readable  */
    if(!fileName.isEmpty() )
    {
        QFile file( fileName );
        if (!file.open(QIODevice::ReadOnly))
        {
            QMessageBox::information(this, tr("Unable to open file"),
            file.errorString());
        }
        else /**<  file ok: make  object Bill*/
        {
            file.close();
            if  (bill != nullptr)
            {
                delete bill;
            }
            try
            {
                bill = new Bill( fileName  );
                if( bill )
                {
                    ui->pushButton->setEnabled(true );
                    emit make_calculation();
                }
            }
            catch (...)
            {
                qInfo()<<"Bill was not created";
            }
        }
    }
} //end of  void MainWindow::open_csv_file()


/** Public slot: action called after loading CSV || after released calculare button
*/
void MainWindow::make_calculation()
{
    const auto start = std::chrono::system_clock::now();

    if(bill )
    {
        QStringList free_numbers = ui->free_numbers->text().simplified().remove(' ').split(',') ;

#if 1   /**<  serial solution  */
         /** get all calls */
        bill->calculate_call_sum( free_numbers,ui->doubleSpin_call_within->value(),
                                                                        true, ui->spin_free_minutes->value() * 60 );
         bill->calculate_call_sum( free_numbers,ui->doubleSpin_call_out->value(),
                                                                      false, ui->spin_free_minutes->value() * 60 );

         /** get all sms */
          bill->calculate_sms_sum( free_numbers,ui->doubleSpin_sms_within->value(),
                                                                          true, ui->spin_free_sms->value() );
          bill->calculate_sms_sum( free_numbers,ui->doubleSpin_sms_out->value(),
                                                                           false, ui->spin_free_sms->value() );

#else   /**<  std::future solution */
        auto f1 = std::async( &Bill::calculate_call_sum,
                        free_numbers,ui->doubleSpin_call_within->value(),
                        true, ui->spin_free_minutes->value() * 60);
        auto f2 = std::async( &Bill::calculate_call_sum,
                        free_numbers,ui->doubleSpin_call_out->value(),
                        false, ui->spin_free_minutes->value() * 60);
        auto f3 = std::async( &Bill::calculate_sms_sum,
                             free_numbers,ui->doubleSpin_sms_within->value(),
                             true, ui->spin_free_sms->value());
        auto f4 = std::async( &Bill::calculate_sms_sum,
                             free_numbers,ui->doubleSpin_sms_within->value(),
                             false, ui->spin_free_sms->value());
        f1.wait();
        f2.wait();
        f3.wait();
        f4.wait();
#endif
        QPair<float, uint>
                call_inside=   bill->get_call_inside(),
                call_outside=bill->get_call_outside(),
                sms_inside=  bill->get_sms_inside(),
                sms_outside=bill->get_sms_outside();

         /** show all results */
         ui->label_air_out_time->setText( Bill::sec_to_hms(call_outside.second  )  );
         ui->label_air_in_time->setText(   Bill::sec_to_hms(call_inside.second )  );
         ui->label_sms_out_counter->setText(QString::number(sms_outside.second ) );
         ui->label_sms_in_counter->setText(QString::number( sms_inside.second)  );

         ui->label_air_out_price->setText(  QString::number(call_outside.first )  );
         ui->label_air_in_price->setText(     QString::number(call_inside.first )  );
         ui->label_sms_out_price->setText(QString::number( sms_outside.first )  );
         ui->label_sms_in_price->setText(  QString::number( sms_inside.first )  );

         ui->label_price->setText(  QString::number  (ui->spin_monthly->value() +
                                         sms_inside.first   +  sms_outside.first  +
                                         call_inside.first +  call_outside.first  )  );

        ui->label_unknown_counter->setText(  QString::number( bill->get_errors() )  );
    }

    const auto diff = std::chrono::system_clock::now() - start;
   qDebug() <<"Done in: "<< std::chrono::duration<double>(diff).count()*1000 << " ms \n";

}


