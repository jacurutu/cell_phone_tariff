#ifndef PHONE_SERVICE_H
#define PHONE_SERVICE_H

#include <QString>
#include <QDebug>

#define CALL_MIN_COUNTED_TIME  ((uint)60)

class Phone_service
{
public:
    Phone_service( QString cdate, QString cphone_number,  uint cquantity, bool cwithin  );
    ~Phone_service();

    QString event_date;
    QString phone_number;
    uint quantity; //sec or units
    bool within=true;


};

#endif // PHONE_SERVICE_H
