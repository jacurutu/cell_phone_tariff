#include "bill.h"


Bill::Bill(QString csv_file): file(csv_file)
{
    /** FillQStringList with values used in csv file
        * works like enum with indexes starting from 0
        *   0:  VS (call within the same operator)
        *   1:  VM (call to different operator)
        *   2:  SS (SMS within the same  operator)
        *   3:  SM (SMS to different operator)
        *  :: add new values here ::
        */
    servise_type  <<  "VS" <<  "VM" << "SS" << "SM";

    /** try to handle csv file*/
    try
    {
        qInfo()<< "Loading: " << csv_file;
        if (file.open(QIODevice::ReadOnly | QIODevice::Text))
        {
            QTextStream in(&file);
            /** skipp header in csv file */
            if(skipp_csv_header)
            {
                 in.readLine();
            }
            while (!in.atEnd())
            {
                QString line = in.readLine();
                process_one_line_from_csv(line);
            } //end of while
        } //end of if; open
    }
    catch (std::bad_alloc& ba)
    {
        qInfo() << "Bill, bad_alloc: " << ba.what() ;
        throw;
    }
    catch (QString reason)
    {
        qInfo() << "Bill from csv file was not created; reason: " <<  reason;
        throw;
    }
    catch (...)
    {
        qInfo() << "Bill from csv file was not created" ;
        throw;
    }
    file.close();
    if(DEB_PRINT){ qInfo()<< "Bill was created" ; }
} //end of Bill::Bill(QString csv_file): file(csv_file)


Bill::~Bill()
{
    if(DEB_PRINT){ qInfo()<< "Bill was closed" ; }
}


/** Method extracts data from 1line in csv file
 * \param  line: just a line
 */
void Bill::process_one_line_from_csv(QString line )
{
    QStringList  rec_list=line.split( csv_delimiter );
#if 0
    static  int line_no=0;
    qInfo() <<"Type : "
                  << servise_type.indexOf( rec_list.at(3 )  )
                  << "; size of list: " <<  rec_list.size()
                  << "; line no. " <<  QString::number( ++ line_no)  ;
#endif
    if( rec_list.size() == 4 )
    {
        switch( servise_type.indexOf( rec_list.at( 3  ) ) )
        {
        case 0:   /**<  VS (call within the same operator) */
            calls.append(new Phone_service( rec_list.at(0),rec_list.at(1),
                                                                            rec_list.at(2).toInt(),true ) );
            break;
        case 1:   /**<    VM (call to different operator)*/
            calls.append(new Phone_service( rec_list.at(0),rec_list.at(1),
                                                                             rec_list.at(2).toInt(),false ) );
            break;
        case 2:   /**<   SS (SMS within the same  operator)*/
            sms.append(new Phone_service( rec_list.at(0),rec_list.at(1),1, true) );
            break;
        case 3:   /**<   SM (SMS to different operator) */
            #if SET_ERR
                if(DEB_PRINT){qInfo("** Type: SM; throw exception") ;}
                throw QString("testeting error");
            #else
                 sms.append(new Phone_service( rec_list.at(0),rec_list.at(1),1, false) );
            #endif
            break;
        default:  /**<  unknown */
            csv_read_errors++;
            break;
        }
    }
    /** wrong number of columns in a csv line */
    else
    {
        csv_read_errors++;
    }
}

/** Method makes summary in  list of Phone_service:sms
 *    Calculated value is stored in protected member variables
 * \param free_numbers: skipped phone numbers
 * \param price_per_unit: price of 1 sms
 * \param inside_network: filter: send inside/outside operator's network
 * \param free_units: do not count these units (do not check targeted network)
 */
void Bill::calculate_sms_sum(
            QStringList free_numbers, float price_per_unit,
             bool inside_network, uint free_units )
 {
     float price_sum=0.0;
     uint quantity_sum=0;
     Phone_service *record=nullptr;
     for( auto  c = sms.begin(); c != sms.end(); c++  )
     {
         record = *c;
         /** summary of time is independent and counted first (in sec) */
         if( record->within == inside_network )
         {
             quantity_sum ++;
         }

         /** skipp free numbers */
         if(free_numbers.contains(record->phone_number) )
         {
             continue;
         }

         /** hadnel free time for calling (variable is in seconds) */
         if(free_units)
         {
                 free_units--;
                 continue;
         } //end of if(free_sec)

         /** count only calls inside||outside network; specified by parameter */
         if( record->within == inside_network )
         {
             price_sum += price_per_unit ;
         }
     } //end of for

     /** store the result  */
     auto   result = qMakePair(price_sum, quantity_sum );
     if(inside_network)
     {
             sms_inside = result;
     }
     else
     {
             sms_outside = result;
     }
 } //end of  Bill::get_sms_sum


/** Method makes summary in  list of : calls;
 *   Calculated value is stored in protected member variables
 * \param free_numbers: skipped phone numbers
 * \param price_per_minute: price per 1 minute
 * \param inside_network: filter: calls inside/outside operator's network
 * \param free_sec: do not count these time (do not check targeted network)
 */
void Bill:: calculate_call_sum(
            QStringList free_numbers, float price_per_minute,
            bool inside_network=true, uint free_sec=0 )
{
    float price_sum=0.0;
    uint quantity_sum=0;
    Phone_service *record=nullptr;
    for( auto  c = calls.begin(); c != calls.end(); c++  )
    {
        record = *c;
        /** summary of time is independent and counted first (in sec) */
        if( record->within == inside_network )
        {
            quantity_sum += record->quantity;
        }

        /** skipp free numbers */
        if(free_numbers.contains(record->phone_number) )
        {
            continue;
        }

        /** handle minimum time for single call (in seconds)*/
        uint counted_time = std::max(CALL_MIN_COUNTED_TIME,
                                                              record->quantity  );

        /** hadnel free time for calling (variable is in seconds) */
        if(free_sec)
        {
            if(free_sec >= counted_time  )
            {
                free_sec -=  counted_time;
                continue;
            }
            else
            {
                counted_time -=  free_sec;
                free_sec=0;
            }
        } //end of if(free_sec)

        /** count only calls inside||outside network; specified by parameter */
        if( record->within == inside_network )
        {
            price_sum += counted_time*price_per_minute/60 ;
        }
    } //end of for

    /** store the result  */
    auto   result = qMakePair(price_sum, quantity_sum );
    if(inside_network)
    {
            call_inside = result;
    }
    else
    {
            call_outside = result;
    }
} //end of  Bill::get_call_sum


/** Static method: transfer time in sec to format 1 h:2m:3s
 *  \return Transfered qstring
 */
QString  Bill::sec_to_hms( uint sec_origin )
 {
     QString ret;
     bool minutes_needed=false;
     if( sec_origin > 3600 )
     {
         ret.append(QString::number(sec_origin/3600) ).append("h:");
         sec_origin = sec_origin %  3600;
         minutes_needed=true;
     }
     if( sec_origin > 60 || minutes_needed )
     {
         ret.append(QString::number(sec_origin/60) ).append("m:");
         sec_origin = sec_origin %  60;
     }
      ret.append(QString::number(sec_origin) ).append("s");

     return  ret;
 }


/** Retutrn value of private member variable
 * \return   csv_read_errors
 */
uint  Bill::get_errors()
{
    return  csv_read_errors;
}


