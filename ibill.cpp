#include "ibill.h"

iBill::iBill()
{
    //qInfo()<< "iBill construct";
}


iBill::~iBill()
{
    /** call destructors for all objects: Phone_service */
    for( auto p=calls.begin();  p != calls.end(); p++ ){delete *p; }
    for( auto p=sms.begin();  p != sms.end(); p++ ){delete *p; }
    calls.clear();
    sms.clear();
    //qInfo()<< "iBill destuctor";
}


/** Calculate all prices of all services
    * \return summary of all services in float
    */
float iBill::get_price_sum()
{
    return  call_outside.first + call_inside.first  +
                 sms_outside.first + sms_inside.first;
}
