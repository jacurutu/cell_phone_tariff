#ifndef IBILL_H
#define IBILL_H

#include <QDebug>
#include <QPair>
#include "phone_service.h"


class iBill
{
public:
    iBill();
    virtual  ~iBill();
    virtual void nope()=0;

    QPair<float, uint>  get_call_inside(){return  call_inside; }
    QPair<float, uint>  get_call_outside(){return  call_outside; }
    QPair<float, uint>  get_sms_inside(){return  sms_inside; }
    QPair<float, uint>  get_sms_outside(){return  sms_outside; }
    virtual float get_price_sum();

protected:
    QList< Phone_service *>calls;
    QList< Phone_service *>sms;
    QPair<float, uint> call_inside, call_outside, sms_inside, sms_outside;
};

#endif // IBILL_H
