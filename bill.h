#ifndef BILL_H
#define BILL_H

#include <QDebug>
#include <QString>
//#include <string> //try-catch
#include <QStringList>
#include <QFile>
#include <QList>
#include <QPair>
#include <algorithm>    // std::max
#include <chrono>
#include "phone_service.h"
#include "ibill.h"

#define DEB_PRINT 0
#define SET_ERR 0
class  Phone_service;


class Bill: public iBill
{
public:
    Bill(QString csv_file);
    virtual void nope(){};
    void calculate_call_sum(
                QStringList free_numbers, float price_per_minute,
                bool inside_network, uint free_sec );
    void calculate_sms_sum(
                QStringList free_numbers, float price_per_unit,
                bool inside_network, uint free_units );
    static QString  sec_to_hms( uint sec_origin );
    uint get_errors();
    ~Bill();

private:
    QFile file;
    bool skipp_csv_header=true;
    char csv_delimiter = ';';
    uint csv_read_errors=0;
    void process_one_line_from_csv(QString line );
    QStringList servise_type;

};

#endif // BILL_H
