#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QString>
#include <QFile>
#include <QPair>
#include <future>
#include <thread>
#include <vector>
#include "bill.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void open_csv_file ();
    void make_calculation();

private:
    Ui::MainWindow *ui;
    Bill *bill = 0 ;
};
#endif // MAINWINDOW_H
